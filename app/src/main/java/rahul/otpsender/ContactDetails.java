package rahul.otpsender;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.SmsManager;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.amulyakhare.textdrawable.TextDrawable;
import com.google.gson.Gson;

import rahul.otpsender.model.Message;

/*
* activity to show the details of a contact - name and number
* allow you to send otp with one click - SendMessage
* */
public class ContactDetails extends AppCompatActivity {
    private TextView textViewName;
    private TextView textViewNumber;
    private Button button;  // sent button
    private String nameOfContact = null;    // name of contact
    private  String numberOfContact = null; // number of contact
    private SharedPreferences sharedPreferences;
    private Message message;
    private ImageView userImageView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_details);

        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        toolbar.setTitle("Contact Details");
        toolbar.setNavigationIcon(R.drawable.back_arrow);
        setSupportActionBar(toolbar);

        Intent intent = getIntent();
        nameOfContact = intent.getStringExtra("nameOfContact");
        numberOfContact = intent.getStringExtra("numberOfContact");

        sharedPreferences = getSharedPreferences(getString(R.string.sharedPrefForRes),MODE_PRIVATE);
        int color = sharedPreferences.getInt(nameOfContact+numberOfContact,Color.GRAY);
        String[] firstLast= intent.getStringExtra("nameOfContact").split(" ", 2);
        String initials = firstLast[0].charAt(0)+"";
        if(firstLast.length>1) initials += firstLast[1].charAt(0);
        TextDrawable userImg = TextDrawable.builder().buildRound(initials, color);
        userImageView = (ImageView)findViewById(R.id.user_pic);
        userImageView.setImageDrawable(userImg);

        // Name of the contact
        textViewName = (TextView)findViewById(R.id.nameOfContact);
        textViewName.setText(nameOfContact);

        //number of the contact
        textViewNumber = (TextView)findViewById(R.id.numberOfContact);
        textViewNumber.setText(numberOfContact);

        button = (Button)findViewById(R.id.sendButton);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendOTP(numberOfContact, nameOfContact);
                // set result =1 (=> otp is sent) to main activity
                setResult(1);
                finish();
            }
        });
    }

    //send sms using phone sms service
    protected void sendOTP(String number,String name){
        try{
            String sms = getString(R.string.otp_message);
            
            // generate six digit OTP
            String OTP = "";
            for(int i=0;i<6;i++){
                OTP += (int)(Math.random() * 10);
            }

            sms += OTP;

            // unix timestamp of sending message
            long timestamp =  System.currentTimeMillis();
            String unixTime = Long.toString(timestamp);

            Gson gson = new Gson();
            message = new Message(name,OTP,timestamp,number);

            //parsing message object to json string to save in sharedPreferences
            String jsonString = gson.toJson(message);

            //sending message
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(number, null, sms, null, null);

            storeMessage(unixTime, jsonString);
        }catch (Exception e){
            String otp_failed = getString(R.string.otp_failed);
            Toast.makeText(this,otp_failed,Toast.LENGTH_LONG).show();
        }
    }

    //  store messages in shared preferences in josn format
    protected void storeMessage(String key, String value){
        sharedPreferences = getSharedPreferences(getString(R.string.sharedPrefKey), MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key,value);
        editor.commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
