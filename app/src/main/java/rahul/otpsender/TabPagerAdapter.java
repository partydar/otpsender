package rahul.otpsender;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
/**
 * adapter for swipable fragments
 */
public class TabPagerAdapter extends FragmentPagerAdapter{

    private ContactFragment contactFragment;
    private SentMessagesFragment sentMessagesFragment;
    private int tabCount;


    public TabPagerAdapter(FragmentManager fm,int tabCount){
        super(fm);
        contactFragment = new ContactFragment();
        sentMessagesFragment = new SentMessagesFragment();
        this.tabCount = tabCount;
    }
    @Override
    public Fragment getItem(int index){
        switch (index){
            case 0:
                // Contact activity
                return contactFragment;
            case 1:
                // Sent messages activity
                return sentMessagesFragment ;
        }
        return null;
    }
    @Override
    // number to tabs
    public int getCount(){
        return tabCount;
    }
}
