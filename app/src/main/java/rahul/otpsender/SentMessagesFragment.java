package rahul.otpsender;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map;

import rahul.otpsender.model.Message;

;

/*
* fragment to show the history of otp sent
* */
public class SentMessagesFragment extends Fragment{

    private ListView listView;
    private SharedPreferences sharedPreferences;
//    private ArrayAdapter<String> adapter;
    private MessageListAdapter adapter;
    private Gson gson;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.activity_sent_messages_fragment, container, false);
        listView = (ListView)rootView.findViewById(R.id.msgList);
        adapter = new MessageListAdapter(getActivity(),new ArrayList<Message>(),getResources());
        listView.setAdapter(adapter);
        refresh();
        return rootView;
    }

    // fetch the data from shared preferences
    public void refresh(){
        try {
            sharedPreferences = getActivity().getSharedPreferences(getString(R.string.sharedPrefKey), Context.MODE_PRIVATE);
            Map<String,?> sentMessages = sharedPreferences.getAll();
            ArrayList keyList = new ArrayList(sentMessages.keySet());
            Collections.sort(keyList,Collections.reverseOrder());
            ArrayList<Message> arrayListMessages = new ArrayList();
            Iterator iterator = keyList.iterator();

            while(iterator.hasNext()){
                String key = (String)iterator.next();
                String jsonVal = (String)sentMessages.get(key);
                gson = new Gson();
                Message message = gson.fromJson(jsonVal, Message.class); // parsing jsonstring to message objecy
                arrayListMessages.add(message);
            }
            adapter.clear();
            adapter.addAll(arrayListMessages);
            adapter.notifyDataSetChanged();

        }catch(NullPointerException e){
            e.printStackTrace();
        }

    }

    // if otp is sent the contactDetails activity sends sent status and if status is true this function is called to relaod the data from shared preferences
    public void onOTPSent(){
        refresh();
    }
}


