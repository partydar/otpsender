package rahul.otpsender;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import rahul.otpsender.model.Contact;

/*
* fragment to show the list of contacts parsed from json file
* */
public class ContactFragment extends Fragment {

    private ListView listview;
    private ContactListAdapter adapter;
    private Gson gson;
    private ArrayList<Contact> contactList;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        View rootView = inflater.inflate(R.layout.activity_contact_fragment, container, false);

        // parsing contact details from json file stored in assets/
        String json = null;
        try{
            InputStream is = getActivity().getAssets().open("contacts.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer,"UTF-8");
        }catch (IOException e){
            e.printStackTrace();
            return null;
        }

        contactList = new ArrayList<Contact>();
        try{
            JSONObject jsonObject = new JSONObject(json);
            JSONArray jsonArray = jsonObject.optJSONArray("contacts");
            for(int i=0;i<jsonArray.length();i++){
                JSONObject jsonObjectI = jsonArray.getJSONObject(i);
                String name = jsonObjectI.optString("name").toString();
                String number = jsonObjectI.optString("number").toString();
                Contact tmpContact = new Contact(name,number);
                contactList.add(tmpContact);

            }
        }catch (JSONException e){
            e.printStackTrace();
        }


        listview = (ListView) rootView.findViewById(R.id.conactList);
        // adapter for to list items
        adapter = new ContactListAdapter(getActivity(),contactList,getResources());
        // setting adapter to listview
        listview.setAdapter(adapter);

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // intent to contactDetails activity to send name and number of contact
                Intent intent = new Intent(ContactFragment.this.getActivity(), ContactDetails.class);
                intent.putExtra("nameOfContact",contactList.get(position).getName());
                intent.putExtra("numberOfContact", contactList.get(position).getPhoneNumber());
                startActivityForResult(intent, 0);
            }
        });

        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }
}
