package rahul.otpsender.model;

import java.io.Serializable;

/**
 * Created by MAD on 10/2/2016.
 */
public class Message implements Serializable{

    private String name;
    private String OTP;
    private long timestamp;
    private String phoneNumber;

    public Message(String name, String OTP, long timestamp,String phoneNumber){
        this.name = name;
        this.OTP = OTP;
        this.timestamp = timestamp;
        this.phoneNumber = phoneNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOTP() {
        return OTP;
    }

    public void setOTP(String OTP) {this.OTP = OTP;}

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
