package rahul.otpsender;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;

/*
Main activity that contains two fragment - one for list of contacts and other for list of sent otp
*/

public class MainActivity extends AppCompatActivity  {
    private ViewPager viewPager; // view pager to swip between tabs
    private TabPagerAdapter tabPagerAdapter;
    private TabLayout tabLayout;

    //Tab titles
    private String[] tabTitles = {"Contacts", "Sent OTPs"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        toolbar.setTitle("OTP Sender");
        setSupportActionBar(toolbar);
        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        tabLayout.addTab(tabLayout.newTab().setText("Contacts"));
        tabLayout.addTab(tabLayout.newTab().setText("Sent OTPs"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        viewPager = (ViewPager)findViewById(R.id.pager);
        tabPagerAdapter = new TabPagerAdapter(getSupportFragmentManager(), 2);
        viewPager.setAdapter(tabPagerAdapter);
//        actionBar.setHomeButtonEnabled(false);

        // setting titles for each tab
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @Override
    // receiving the opt sms sending status send by ContactDetails fragment
    public void onActivityResult(int requestCode, int resultCode, Intent otpStatus){
        super.onActivityResult(requestCode, resultCode, otpStatus);
        if(resultCode==1){
            Toast.makeText(this, "OTP Sent", Toast.LENGTH_LONG).show();
            if(viewPager!=null){
                // opt sent in sentMessageFragment
                ((SentMessagesFragment)tabPagerAdapter.getItem(1)).onOTPSent();  ;
            }
        }
    }
    public TextDrawable textDrawable(String name){
        ColorGenerator colorGenerator = ColorGenerator.MATERIAL;
        int color = colorGenerator.getRandomColor();
        String[] firstLast= name.split(" ", 2);
        String initials = firstLast[0].charAt(0)+"";
        if(firstLast.length>1) initials += firstLast[1].charAt(0);
        //System.out.println(initials);
        TextDrawable userImg = TextDrawable.builder().buildRound(initials, color);
        return userImg;
    }


}
