package rahul.otpsender;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import rahul.otpsender.model.Message;

/**
 * customised adapter to show the list of sent otps along with the name of contact and time of seding otp
 *
 */
public class MessageListAdapter extends BaseAdapter{
    private ArrayList<Message> dataSet; // otp history in the form of list of message object
    private Context context;
    private LayoutInflater inflater = null;
    private Message msg;
    private Resources resources;
    private SharedPreferences sharedPreferences;
    // initialising the adapter
    public MessageListAdapter(Context context, ArrayList<Message> dataSet, Resources resources){
        this.context = context;
        this.dataSet = dataSet;
        this.resources = resources;
        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    // total number to messages sent
    public int getCount(){
        if(dataSet.size()<=0){
            return  1;
        }
        return  dataSet.size();
    }
    public Object getItem(int position){
            return  position;
    }
    public long getItemId(int position){
        return  position;
    }

    // class of listview
    public class ViewHolder{
        TextView nameTextView;
        TextView timeTextView;
        TextView otpTextView;
        ImageView clockImageView;
        ImageView otpImageView;
        ImageView userImageView;
    }

    // add complete otp history to adapter
    public void addAll(ArrayList<Message> dataSet){
        this.dataSet = dataSet;
    }

    // remove complete history from adapter
    public void clear(){
        this.dataSet = null;
    }

    // add list items to listview
    public View getView(int position, View convertView, ViewGroup parent){
        View view = convertView;
        ViewHolder viewHolder;
        if(convertView==null){
            view = inflater.inflate(R.layout.row_item,parent,false);
            viewHolder = new ViewHolder();
            viewHolder.nameTextView = (TextView)view.findViewById(R.id.name);
            viewHolder.timeTextView = (TextView)view.findViewById(R.id.time);
            viewHolder.otpTextView = (TextView)view.findViewById(R.id.otp);
            viewHolder.clockImageView = (ImageView)view.findViewById(R.id.clock);
            viewHolder.otpImageView = (ImageView)view.findViewById(R.id.otpImg);
            viewHolder.userImageView = (ImageView)view.findViewById(R.id.userPicList);
            view.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder)view.getTag();
        }
        if(dataSet.size()>0){
            msg = null;
            msg = dataSet.get(position);
            SimpleDateFormat sdf = new SimpleDateFormat(resources.getString(R.string.date_format));
            Date resultdate = new Date(msg.getTimestamp());
            String timeString = sdf.format(resultdate);
            viewHolder.nameTextView.setText(msg.getName());
            viewHolder.timeTextView.setText(timeString);
            viewHolder.otpTextView.setText(String.valueOf(msg.getOTP()));
            viewHolder.clockImageView.setImageResource(resources.getIdentifier("rahul.otpsender:drawable/clock", null, null));
            viewHolder.otpImageView.setImageResource(resources.getIdentifier("rahul.otpsender:drawable/otp_img",null,null));


            sharedPreferences = context.getSharedPreferences(resources.getString(R.string.sharedPrefForRes),Context.MODE_PRIVATE);
            int color = sharedPreferences.getInt(msg.getName()+msg.getPhoneNumber(), Color.GRAY);
            String[] firstLast= msg.getName().split(" ", 2);
            String initials = firstLast[0].charAt(0)+"";
            if(firstLast.length>1) initials += firstLast[1].charAt(0);
            TextDrawable userImg = TextDrawable.builder().buildRound(initials, color);
            viewHolder.userImageView.setImageDrawable(userImg);
        }
        return view;
    }
}
