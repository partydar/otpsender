package rahul.otpsender.utils;

import android.content.res.Resources;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.ActionBar;

import rahul.otpsender.R;

/**
 * Created by MAD on 10/3/2016.
 */
public class Common {
    public static void setActionBar(ActionBar actionBar, Resources resources){
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_TITLE);
        actionBar.setBackgroundDrawable(new ColorDrawable(resources.getColor(R.color.action_bar_color)));
        actionBar.setStackedBackgroundDrawable(new ColorDrawable(resources.getColor(R.color.action_bar_color)));
    }
}
