package rahul.otpsender;

/**
 * Created by MAD on 10/3/2016.
 */

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;

import java.util.ArrayList;

import rahul.otpsender.model.Contact;


/**
 * customised adapter to show the list of sent otps along with the name of contact and time of seding otp
 *
 */
public class ContactListAdapter extends BaseAdapter {
    private ArrayList<Contact> dataSet; // otp history in the form of list of message object
    private Context context;
    private LayoutInflater inflater = null;
    private Contact contact;
    private Resources resources;
    private SharedPreferences sharedPreferences;
    private String name;
    private String phoneNumber;
    // initialising the adapter
    public ContactListAdapter(Context context, ArrayList<Contact> dataSet, Resources resources){
        this.context = context;
        this.dataSet = dataSet;
        this.resources = resources;
        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    // total number to messages sent
    public int getCount(){
        if(dataSet.size()<=0){
            return  1;
        }
        return  dataSet.size();
    }
    public Object getItem(int position){
        return  position;
    }
    public long getItemId(int position){
        return  position;
    }

    // class of listview
    public class ViewHolder{
        TextView nameTextView;
        TextView numberTextView;
        ImageView userImageView;
    }

    // add complete otp history to adapter
    public void addAll(ArrayList<Contact> dataSet){
        this.dataSet = dataSet;
    }

    // remove complete history from adapter
    public void clear(){
        this.dataSet = null;
    }

    // add list items to listview
    public View getView(int position, View convertView, ViewGroup parent){
        View view = convertView;
        ViewHolder viewHolder;
        if(convertView==null){
            view = inflater.inflate(R.layout.contact_row,parent,false);
            viewHolder = new ViewHolder();
            viewHolder.nameTextView = (TextView)view.findViewById(R.id.nameOfContactInList);
            viewHolder.numberTextView = (TextView)view.findViewById(R.id.numberOfContactInList);
            viewHolder.userImageView = (ImageView)view.findViewById(R.id.userPicList);
            view.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder)view.getTag();
        }
        if(dataSet.size()>0){
            contact = null;
            contact = dataSet.get(position);
            name = contact.getName();
            phoneNumber = contact.getPhoneNumber();
            viewHolder.nameTextView.setText(name);
            viewHolder.numberTextView.setText(phoneNumber);

            ColorGenerator colorGenerator = ColorGenerator.MATERIAL;
            int color = Color.WHITE;

            sharedPreferences = context.getSharedPreferences(resources.getString(R.string.sharedPrefForRes), Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            if(sharedPreferences.contains(name+phoneNumber)){
                color = sharedPreferences.getInt(name+phoneNumber, Color.GRAY);
            }else{
                color = colorGenerator.getRandomColor();
                editor.putInt(contact.getName() + contact.getPhoneNumber(), color);
                editor.commit();
            }

            String[] firstLast= contact.getName().split(" ", 2);
            String initials = firstLast[0].charAt(0)+"";
            if(firstLast.length>1) initials += firstLast[1].charAt(0);
            TextDrawable userImg = TextDrawable.builder().buildRound(initials, color);
            viewHolder.userImageView.setImageDrawable(userImg);

            // storing color in shared preferences

        }
        return view;
    }
}

