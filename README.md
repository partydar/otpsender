# OTPSender #
Android App to send OTP


### AIM 
To send OTP (One Time Password) to a phone number.

### Platform Requirement 
*	OS : Android
*	Minimum SDK Version : 16
*	Target SDK Version : 22 ,Tested on SDK Version : 22 and 23

### Product Components 
*	List of Contacts 
*	History of message sent
*	Details View of Contact
*	Sending an OTP to Contact

### List Of Contacts 
*	Raw contacts are stored in JSON format with structure:                                         
{
“name” : ”Name of Contact”,                                             
”number” : ”Phone Number of Contact”
},
*	ArrayAdapter is used to bind contacts to ListView
*	List display only names of contacts
*	On clicking a contact it will take you to Contact Details view.

### History of Message Sent
*	Sent messages are shown in the form of ListView in descending order of Time of OTP sending
*	Custom Adapter is designed to bind the History of OTP sent. 
*	All components – Name, Time of OTP Sending, OTP are bound to separate TextViews to give them different styling
*	Message Model is used to represent a message
*	Storing OTP History : OTP history is stored in the phone with sharedPreferences in JSON format
*	unixTimeStamp is used as key to store message in sharedPreferences
*	Gson (Library by google to handle operation related to JSON) library is used to parse JSON from and to Message Object
*	Structure of message stored  
{ “name”: “Name of Contact”,
“timestamp” : ”Time In Millis, unixTimeStamp”,
“OTP” : “six digit OTP” }

### Details of Contact  
*	Details of Contact include Name, Phone number
*	It also contains a button which on click sends SMS to contact 
*	Structure of SMS : Hi. Your OTP is: XXXXXX
*	Six digit OTP is generated using Math.random method of java


### Sending OTP  
*	SmsManager is used to send SMS to contact from users phone(which has OTPSender installed). SMS charges are charged by Servicer Provider.
*	After sending SMS app take back user to Main Activity(Contact List)
*	Simultaneous update of List of OTP history is maintained by checking the status of last OTP sent. If OTP is sent successfully ContactDetails activity send status to Main activity and accordingly it will refresh the OTP history

### Library Used 
*	Gson : To handle JSON operations 
https://github.com/google/gson
*	References which are used for learning in development : 
	Official Android Doc https://developer.android.com/training/index.html
	TutorialsPoint 
http://www.tutorialspoint.com/android/
	For custom adapter
 https://goo.gl/dVysTw
	Google

### Practices
*	Used JOSN to store messages
*	Used model Message to store the message details
*	Different styling for Name, Time, OTP in List of OTP sent



### Decision for choosing Android over web
*	Kisan Network provides android application for farmers and buyers, so I thought If I get selected I would be working of android so why not with android
*	Good to learn new things (beginner in android development)